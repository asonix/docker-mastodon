ARG REPO_ARCH

FROM $REPO_ARCH/ruby:3.2-alpine3.18

# Set up git remote
ARG GIT_BRANCH
ARG GIT_REPOSITORY

ARG REPO_ARCH

# Tell rails to serve static files
ENV RAILS_SERVE_STATIC_FILES="true"
ENV RAILS_ENV="production"
ENV NODE_ENV="production"

# Install helper scripts
COPY root/ /

# Install build dependencies
RUN apk add --no-cache whois nodejs yarn ca-certificates git bash \
  gcc g++ make libc-dev file sed \
  imagemagick protobuf-dev libpq ffmpeg icu-dev libidn-dev yaml-dev \
  readline-dev postgresql-dev curl tini python3 shared-mime-info && \
  if [ "${REPO_ARCH}" = "arm64v8" ]; then apk add libc6-compat; else echo "${REPO_ARCH} is not arm64v8, skipping compat"; fi && \
  update-ca-certificates

# Create the mastodon user
ARG UID=1001
ARG GID=1001
RUN echo "Etc/UTC" > /etc/localtime && \
  mkdir -p /opt/mastodon && \
  addgroup --gid $GID mastodon && \
  adduser -D -u $UID -G mastodon -h /opt/mastodon mastodon && \
  echo "mastodon:`head /dev/urandom | tr -dc A-Za-z0-9 | head -c 24 | mkpasswd -s -m sha-256`" | chpasswd && \
  chown -R mastodon:mastodon /opt/mastodon

# Install mastodon
USER mastodon
RUN git clone -b ${GIT_BRANCH} ${GIT_REPOSITORY} /opt/mastodon && \
  rm -rf /opt/mastodon/.git

# Install dependencies
WORKDIR /opt/mastodon
RUN mkdir -p /opt/mastodon/.config/yarn/global && \
  touch /opt/mastodon/.config/yarn/global/.yarnclean && \
  gem install bundler && \
  bundle config set --local deployment 'true' && \
  bundle config set --local without 'development test' && \
  bundle config set silence_root_warning 'true' && \
  bundle install -j$(nproc) && \
  yarn install --pure-lockfile --production --network-timeout=30000

# Compile assets
RUN OTP_SECRET=precompile_placeholder SECRET_KEY_BASE=precompile_placeholder \
  bundle exec rails assets:precompile

ENTRYPOINT ["/sbin/tini", "--"]
